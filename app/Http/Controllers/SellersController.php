<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;

class SellersController extends ApiController
{

    public function index()
    {
        $sellers = Seller::has('products')->get();
        //return response()->json(['count'=>$sellers->count(), 'data'=> $sellers],200);
        return $this->showAll($sellers);
    }

    public function show(Seller $seller)
    {
        //return response()->json(['data'=>$seller],200);
        return $this->showOne($seller);
    }


}
