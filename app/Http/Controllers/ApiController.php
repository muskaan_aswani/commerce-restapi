<?php

namespace App\Http\Controllers;

use App\Traits\ResponseHelper;


class ApiController extends Controller
{
    use ResponseHelper;

    //We created this Controller so that we could make the code more clean and readable
    //The Traits folder has a ResponseHelper in which we have created functions that are used repeatedly
    //Hence to make code more general we created ApiController and ResponseHelper
}
