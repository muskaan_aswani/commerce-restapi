<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seller;

class SellerCategoryController extends ApiController
{
    public function index(Seller $seller){
        $categories = $seller->products()
            ->whereHas('catgeories')
            ->with('categories')
            ->get()
            ->pluck('categories')
            ->flatten()
            ->unique('id')
            ->values();

        return $this->showAll($categories);
    }
}
