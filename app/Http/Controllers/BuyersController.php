<?php

namespace App\Http\Controllers;

use App\Models\Buyer;
use Illuminate\Http\Request;

class BuyersController extends ApiController
{

    public function index()
    {
        $buyers = Buyer::has('transactions')->get();
        //return response()->json(['count'=>$buyers->count(), 'data'=> $buyers],200);
        return $this->showAll($buyers); //Coming from ApiController's Trait
    }


    public function show(Buyer $buyer)
    {
        //$buyer = Buyer::has('transactions')->findOrFail($id);
        //return response()->json(['data'=>$buyer],200);
        return $this->showOne($buyer);
    }


}
