<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::resource('buyers',App\Http\Controllers\BuyersController::class)->only('index','show');

Route::resource('sellers',App\Http\Controllers\SellersController::class)->only('index','show');
Route::resource('sellers.transactions',App\Http\Controllers\SellerTransactionController::class)->only('index');
Route::resource('sellers.categories',App\Http\Controllers\SellerCategoryController::class)->only('index');
Route::resource('sellers.buyers',App\Http\Controllers\SellerBuyerController::class)->only('index');
Route::resource('sellers.products',App\Http\Controllers\SellerProductController::class)->except('edit','create','show');

Route::resource('users',App\Http\Controllers\UsersController::class)->except('create','edit');


Route::resource('categories',App\Http\Controllers\CategoriesController::class)->except('create','edit');
Route::resource('categories.products',App\Http\Controllers\CategoryProductController::class)->only('index');
Route::resource('categories.sellers',App\Http\Controllers\CategorySellerController::class)->only('index');
Route::resource('categories.transactions',App\Http\Controllers\CategoryTransactionController::class)->only('index');
Route::resource('categories.buyers',App\Http\Controllers\CategoryBuyerController::class)->only('index');



Route::resource('products',\App\Http\Controllers\ProductsController::class)->only('index','show');
Route::resource('transactions',\App\Http\Controllers\TransactionsController::class)->only('index','show');

Route::resource('transactions.categories',\App\Http\Controllers\TransactionCategoryController::class)->only('index');
Route::resource('transactions.seller',\App\Http\Controllers\TransactionSellerController::class)->only('index');
Route::resource('buyers.transactions',\App\Http\Controllers\BuyerTransactionController::class)->only('index');
Route::resource('buyers.products',\App\Http\Controllers\BuyerProductController::class)->only('index');
Route::resource('buyers.sellers',\App\Http\Controllers\BuyerSellerController::class)->only('index');
Route::resource('buyers.categories',\App\Http\Controllers\BuyerCategoryController::class)->only('index');
